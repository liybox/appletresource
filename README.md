# 爱i分享小程序
## 一.项目介绍
### 这是一款提供数字资源的微信小程序，用户可以通过复制网盘链接和提取码的方式免费获取资源。
### 小程序提供虚拟资源的具体种类
- 技能类：普通话视频教程/动态表情包设计视频教程/校园恋爱教程/约会教程/国内旅游攻略/解压缩密码破解软件/相亲课程,  IT类的教程等

- 素材类：PPT,Word,Excel教程/海报 。毕业答辩，简历模板等

- 求职类：模拟实况视频教学，面部礼仪教学视频，产品/运营/技术岗位笔试题目和经验分享，名企面试资料，谈判技巧视频教学，面试视频教学，应届生系列，自我介绍视频教学，职业规划视频教学等

-  外语类：剑桥真题，葡萄牙语教学，雅思教学，口语学习资料，英语写作高分素材，GRE写作模板，托福等。

-  写作类：国考写作教学，30天论文写作教学，毕业论文写作攻略与教程，网文写作教程，高考语文写作素材，编辑写作教程与素材，玄幻小说写作素材等等。

-  书籍类：60本提高记忆力电子书，知乎高赞回答150本优秀书籍，金融时报年度书籍，纽约客年度书籍2018等

-  影视类：老友记等

-  游戏类：游戏资料大全，270多G游戏资料，整理成10个文件夹

-  IT书：系列书籍：JAVA，Linux, Mysql Php,Python,web ，编程技巧，大数据，汇编语言，网络安全等，每个系列都有10余本，算是比较全的

-  开发：关于开发的视频教学：安卓逆向视频教学，linux嵌入，云计算课程，QT医疗项目开发等等

-  工具：70多款工具，关于IT与编程，设计都能在这里找到。

-  小程序：小程序视频教学，和云开发资料等等

    
## 二.扫码体验

![小程序码](miniprogram/images/mayun/qr.png)


## 三.项目截图
### 首页 ，资源详情页，详情页

<img src="miniprogram/images/mayun/首页.jpg" width="300" height="560" alt="首页" >
<img src="miniprogram/images/mayun/资源详情页.jpg" width="300" height="560" alt="资源详情页" >
<img src="miniprogram/images/mayun/搜索页.jpg" width="300" height="560" alt="搜索页" >


### 我的页面 ，客服页面，帮助返回页


<img src="miniprogram/images/mayun/我的.jpg" width="300" height="560" alt="我的" >
<img src="miniprogram/images/mayun/客服页面.jpg" width="300" height="560" alt="客服页面" >
<img src="miniprogram/images/mayun/帮助返回页.jpg" width="300" height="560" alt="帮助返回页.jpg" >


## 四.项目技术环境
### 云开发
- 数据库：一个既可在小程序前端操作，也能在云函数中读写的 JSON 文档型数据库
- 文件存储：在小程序前端直接上传/下载云端文件，在云开发控制台可视化管理
- 云函数：在云端运行的代码，微信私有协议天然鉴权，开发者只需编写业务逻辑代码
- [云开发文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)

### 小程序UI组件Vant Weapp 
- [Vant Weapp官方文档](https://youzan.github.io/vant-weapp/#/intro)
- 其他组件库参考：
[微信小程序UI组件库合集](https://developers.weixin.qq.com/community/develop/article/doc/000ecc775a86807f7ba9b7dc956c13)

### 客服关键词回复功能介绍
项目中云函数 kefu 中有关键字词自动回复功能，可以查看代码或扫码体验回复“001”，“002” …………
客服关键词回复功能的实现需要在小程序云控制台配置，云函数接收消息推送接口打开，添加消息推送，如图


 ![客服后台设置](miniprogram/images/mayun/客服后台.png)

### 小程序赞赏功能实现
-  方法一：跳转至第三方小程序，如给赞，最近给赞小程序赞赏功能不能使用，原因未知。
-  方法二（推荐）：微信支付中可以申请自己的赞赏码
    赞赏码位置：微信中我的-->支付-->收款码-->赞赏码
## 五.项目目录结构与数据库集合
### 文件目录结构
#### 云函数
    getresource 获取资源
    kefu        客服自定义回复
    qrcode      生成小程序码，生成海报的时候用到

#### components组件
    add_tips            首页中，添加小程序的组件    
    canvasdrawer        画海报的组件
#### 页面
    "pages/start/start",    启动页面，倒计时5秒
    "pages/index/index",    首页
    "pages/detail/detail",  详情页
    "pages/poster/poster",  海报页面
    "pages/kefu/kefu",      客服页面
    "pages/search/search",  搜索页面
    "pages/about/about",    关于程序页面
    "pages/help/help",      帮助中心页面
    "pages/my/my"           我的页面
### 数据库集合
  数据库集合目录如下
  -  banner.json  	首页轮播图
  -  resources.json  资源	
  -  resourcestype.json 资料种类
  -  start.json	  启动页面
  
  数据库集合创建后，一定要修改权限为最大，小程序端才可以读取数据，如图


  ![数据库集合权限](miniprogram/images/mayun/数据库集合权限.png)

## 六.项目参考资料汇总
该项目的完成少不了如下开源社区提供的资料与帮助，如对您有帮助，请点个star
-  [腾讯云：精心整理！小程序开发资源汇总](https://developers.weixin.qq.com/community/develop/article/doc/000a84e7f48350452bc92176651813)
-  [小程序云开发文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)
-  [二手书商城](https://github.com/TencentCloudBase/Good-practice-tutorial-recommended/tree/master/secondhand%20book%20mall%EF%BC%88%E4%BA%8C%E6%89%8B%E4%B9%A6%E5%95%86%E5%9F%8E%EF%BC%89)
-  [知乎高赞：如何入门微信小程序开发，有哪些学习资料](https://www.zhihu.com/question/50907897)
-  [微信小程序UI组件库合集](https://developers.weixin.qq.com/community/develop/article/doc/000ecc775a86807f7ba9b7dc956c13)


## 七.作者与赞赏
###  如果有具体问题，请加微信咨询；如果对您有帮助，请作者喝杯咖啡
<img src="miniprogram/images/mayun/作者.jpg" width="400" height="400" alt="作者" >
<img src="miniprogram/images/mayun/赞赏码.png" width="400" height="400" alt="赞赏码" >