// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  traceUser: true
})

// 云函数入口函数
exports.main = async (event, context) => {
  //先判断云存储是否存在此二维码
  try {
    await cloud.downloadFile({
      fileID: 'share/' + event.scene + '.jpeg',
    })
    console.log('get from cos')
    return 'share/' + event.scene + '.jpeg'
    //不存在则进行生成
  } catch (e) {
    console.log('creat start')
    //先获取
    const bufferContent = await cloud.openapi.wxacode.get({
      path: "pages/detail/detail?scene="+event.scene
    })
    console.log(bufferContent)
    //再上传云存储
    const fileContent = await cloud.uploadFile({
      cloudPath: 'share/' + event.scene + '.jpeg',
      fileContent: bufferContent.buffer
    })
    return fileContent.fileID
  }
}