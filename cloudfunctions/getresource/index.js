// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
  traceUser: true
})
const db = cloud.database();
// 云函数入口函数
exports.main = async(event, context) => {
  const _ = db.command;
  var id=event.id;
  const result= db.collection('resources').doc(id).update({
    // data 传入需要局部更新的数据
    data: {
        downloadnum: _.inc(1)
      
    }
  }).then(res=>{
      console.log(res);
  }).catch(res=>{
    console.log(res);
  })
  return result;
}