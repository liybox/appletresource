var data = {
  //云开发环境id
      env: 'test-s58tn',
  //分享配置
      share_title: '爱分享',
      share_img: '/images/poster.jpg', //可以是网络地址，本地文件路径要填绝对位置
  share_poster:'https://7465-test-s58tn-1301613373.tcb.qcloud.la/share/%E6%89%AB%E7%A0%81_%E6%90%9C%E7%B4%A2%E8%81%94%E5%90%88%E4%BC%A0%E6%92%AD%E6%A0%B7%E5%BC%8F-%E6%A0%87%E5%87%86%E8%89%B2%E7%89%88.png?sign=575e71e0a44fc08f8855a5f48c9c6d2e&t=1584802723',//必须为网络地址
  //客服联系方式
      kefu: {
            weixin: 'Babyqinxin',
            qq: '497461990',
           // gzh: 'https: //mmbiz.qpic.cn/mmbiz_png/nJPznPUZbhpKCwnibUUqnt7BQXr3MbNsasCfsBd0ATY8udkWPUtWjBTtiaaib6rTREWHnPYNVRZYgAesG9yjYOG7Q/640', //公众号二维码必须为网络地址
            phone: '' //如果你不设置电话客服，就留空
  },
  //默认启动页背景图，防止请求失败完全空白 
  //可以是网络地址，本地文件路径要填绝对位置
      bgurl: '/images/start.png'
}
//下面的就别动了
function formTime(creatTime) {
      let date = new Date(creatTime),
            Y = date.getFullYear(),
            M = date.getMonth() + 1,
            D = date.getDate(),
            H = date.getHours(),
            m = date.getMinutes(),
            s = date.getSeconds();
      if (M < 10) {
            M = '0' + M;
  }
      if (D < 10) {
            D = '0' + D;
  }
      if (H < 10) {
            H = '0' + H;
  }
      if (m < 10) {
            m = '0' + m;
  }
      if (s < 10) {
            s = '0' + s;
  }
      return Y + '-' + M + '-' + D + ' ' + H + ':' + m + ':' + s;
}

function days() {
      let now = new Date();
      let year = now.getFullYear();
      let month = now.getMonth() + 1;
      let day = now.getDate();
      if (month < 10) {
            month = '0' + month;
  }
      if (day < 10) {
            day = '0' + day;
  }
      let date = year + "" + month + day;
      return date;
}
module.exports = {
      data: JSON.stringify(data),
      formTime: formTime,
      days: days
}