const app = getApp()
const db = wx.cloud.database();
const config = require("../../config.js");
const _ = db.command;
Page({
  data: {
    college: [],
    collegeCur: -2,
    showList: false,
    scrollTop: 0,
    nomore: false,
    list: [],
    resourcestype:[]
    
  },
  onLoad() {
    this.getresourcetype();
    this.listkind();
    this.getbanner();
   
  },
  /**
   * 获取资源类目
   */
  getresourcetype: function () {

    // db.collection('resourcetype').where({}).get().then(res => {
    //   // res.data 包含该记录的数据
    //   console.log("查询类目")
    //   console.log(res)
    // })
    var that=this;
    db.collection('resourcestype').where({}).get({
      success: function (res) {
        var college=res.data[0].type;
        that.setData({
          college: college
        })
        let resourcestype=that.data.resourcestype;
        for(let i=0;i<college.length;i++){
          resourcestype.push(college[i].id)
        }

        wx.setStorageSync('resourcestype', resourcestype);

        that.setData({
          resourcestype:resourcestype
        },
        that.getList()
        )
      }
    })
  },
  //监测屏幕滚动
  onPageScroll: function (e) {
    this.setData({
      scrollTop: parseInt((e.scrollTop) * wx.getSystemInfoSync().pixelRatio)
    })
  },
  //获取上次布局记忆
  listkind() {
    let that = this;
    wx.getStorage({
      key: 'iscard',
      success: function (res) {
        that.setData({
          iscard: res.data
        })
      },
      fail() {
        that.setData({
          iscard: true,
        })
      }
    })
  },
  //布局方式选择
  changeCard() {
    let that = this;
    if (that.data.iscard) {
      that.setData({
        iscard: false
      })
      wx.setStorage({
        key: 'iscard',
        data: false,
      })
    } else {
      that.setData({
        iscard: true
      })
      wx.setStorage({
        key: 'iscard',
        data: true,
      })
    }
  },
  //跳转搜索
  search() {
    wx.navigateTo({
      url: '/pages/search/search',
    })
  },
  //学院选择
  collegeSelect(e) {
    this.setData({
      collegeCur: e.currentTarget.dataset.id,
      scrollLeft: (e.currentTarget.dataset.id - 3) * 100,
      showList: false,
    })
    console.log("资源类目")
    console.log(this.data.collegeCur);
    this.getList();
  },
  //选择全部
  selectAll() {
    this.setData({
      collegeCur: -2,
      scrollLeft: -200,
      showList: false,
    })
    this.getList();
  },
  //展示列表小面板
  showlist() {
    let that = this;
    if (that.data.showList) {
      that.setData({
        showList: false,
      })
    } else {
      that.setData({
        showList: true,
      })
    }
  },
  getList() {
    let that = this;
    var typeId = -2;
    if (that.data.collegeCur == -2) {
      let resourcestype=that.data.resourcestype;
      typeId =  _.in(resourcestype); //查询在resourcestype类别中的所有
      //typeId = _.neq(-2); //除-2之外所有
    } else {
      typeId = that.data.collegeCur //小程序搜索必须对应格式
    }
    db.collection('resources').where({
        typeId: typeId
    }).get({
      success: function (res) {
        console.log("查询结果")
        console.log(res)

        var listtodo=res.data;
        for(let i=0;i<listtodo.length;i++){
          listtodo[i].logourl=listtodo[i].logourl.trim();
        }

        wx.stopPullDownRefresh(); //暂停刷新动作
        if (listtodo.length == 0) {
          that.setData({
            nomore: true,
            list: [],
          })
          return false;
        }
        if (listtodo.length < 20) {
          that.setData({
            nomore: true,
            page: 0,
            list: listtodo,
          })
        } else {
          that.setData({
            page: 0,
            list: listtodo,
            nomore: false,
          })
        }
      },
      fail: function (res) {
        console.log("查询结果失败")
        console.log(res)
      }
    })
  },
  more() {
    let that = this;
    if (that.data.nomore || that.data.list.length < 20) {
      return false
    }
    let page = that.data.page + 1;
    if (that.data.collegeCur == -2) {
      let resourcestype=that.data.resourcestype;
      var collegeid =  _.in(resourcestype); //查询在resourcestype类别中的所有

      //var collegeid = _.neq(-2); //除-2之外所有
    } else {
      var collegeid = that.data.collegeCur  //小程序搜索必须对应格式
    }
    db.collection('resources').where({
      
        typeId: collegeid
      
    }).skip(page * 20).limit(20).get({
      success: function (res) {
        if (res.data.length == 0) {
          that.setData({
            nomore: true
          })
          return false;
        }
        if (res.data.length < 20) {
          that.setData({
            nomore: true
          })
        } 
        var listtodo=res.data;
        for(let i=0;i<listtodo.length;i++){
          listtodo[i].logourl=listtodo[i].logourl.trim();
        }
        that.setData({
          page: page,
          list: that.data.list.concat(listtodo)
        })
      },
      fail() {
        wx.showToast({
          title: '获取失败',
          icon: 'none'
        })
      }
    })
  },
  onReachBottom() {
    this.more();
  },
  //下拉刷新
  onPullDownRefresh() {
    this.getList();
  },
  gotop() {
    wx.pageScrollTo({
      scrollTop: 0
    })
  },
  //跳转详情
  detail(e) {
    let that = this;
    wx.navigateTo({
      url: '/pages/detail/detail?scene=' + e.currentTarget.dataset.id,
    })
  },
  //获取轮播
  getbanner() {
    let that = this;
    db.collection('banner').where({}).get({
      success: function (res) {
        that.setData({
          banner: res.data[0].list
        })
      }
    })
  },
  //跳转轮播链接
  goweb(e) {
    if (e.currentTarget.dataset.web) {
      app.globalData.notice=e.currentTarget.dataset.notice;
      wx.navigateTo({
        url: '../banner/'+e.currentTarget.dataset.web+'/'+e.currentTarget.dataset.web
      })
    }
  },
  //分享
  onShareAppMessage() {
    return {
      title: '这里有很多免费资源！快来看看吧',
      path: '/pages/index/index'
    }
  }

})