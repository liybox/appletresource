var app = getApp();
var db = wx.cloud.database();

Page({
  /**
   * 页面的初始数据
   */
  data: {
    list: [{
        title: '该程序是做什么的？',
        id: 0,
        des: ['程序整理网上资源并免费分享出来',
          '资源类别：技能，素材，求职，写作，外语，书籍，影视，游戏，IT书，开发，工具，小程序等'
        ],
        check: true,
      }, {
        title: '该程序收费吗？',
        id: 1,
        des: ['不收费'],
        check: false,
      }, {
        title: '如何获取资源',
        id: 2,
        des: ['资源详情页面，点击“获取资源”按钮'],
        check: false,
      },
      {
        title: '如何分享资源',
        id: 3,
        des: ['通过邮件或微信或QQ，把资源发送本人'],
        check: false,
      },
      {
        title: '关于资源',
        id: 4,
        des: ['资源来源于网络，若侵犯到版权，请联系作者，及时删除处理。人工整理资源，若您发现错误，请与本人联系，谢谢！'],
        check: false,
      },
      {
        title: '关联小程序',
        id: 5,
        des: ['你的微信公众号需要与这款小程序关联吗？',
        '关联步骤：', 
        '1.登录微信公众号 ',
        '2.小程序管理->添加 ',
        '3.关联小程序' ,
        '4. 输入爱i分享的小程序信息。 AppID:wxbdde9761b97b3252',
        '5.发送关联邀请，发送邀请后，请微信联系，会尽快处理',
        '6.本关联是自愿原则，无金钱来往'],
        check: false,
      }
    ]
  },
  onReady() {},

  show(e) {
    var that = this;
    let ite = e.currentTarget.dataset.show;
    let list = that.data.list;
    if (!ite.check) {
      list[ite.id].check = true;
    } else {
      list[ite.id].check = false;
    }
    that.setData({
      list: list
    })
  },
  //跳转页面
  go(e) {
    wx.navigateTo({
      url: e.currentTarget.dataset.go
    })
  },
  onLoad() {

  },
  //分享
  onShareAppMessage() {
    return {
          title: '发现一款不错的小程序！快来看看吧',
          path: '/pages/help/help'
    }
}

})