const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
        des: '开发此程序的初衷:汇聚资源，分享资源。\n\n我们在平时的工作/生活/学习中，每个人都会有一些自己独特的资源：技能，素材，求职，写作，外语，书籍，影视，游戏，IT书，开发，工具，小程序等。通过这个小平台整理分享出来，希望大家可以在这里找到自己想要的资源 \n\n感谢开源精神，在创造小程序过程中，获取宝贵帮助\n有任何问题请反馈'

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
        console.log(options)
      var notice=app.globalData.notice;

      this.setData({
            notice:notice
      })
  },

  onReady: function () {

  },
  //复制
  copy(e) {
        wx.setClipboardData({
              data: e.currentTarget.dataset.copy,
              success: res => {
                    wx.showToast({
                          title: '复制' + e.currentTarget.dataset.name + '成功',
                          icon: 'success',
                          duration: 1000,
                    })
              }
        })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  //分享
  onShareAppMessage() {
        return {
              title: '发现一款不错的小程序！快来看看吧',
              path: '/pages/about/about'
        }
  }
})